import datetime
import logging
import sqlite3


class Database:
    logger = logging.getLogger()
    conn = sqlite3.connect('dungeonbot.sqlite', check_same_thread=False)
    cursor = conn.cursor()
    ttl = datetime.timedelta(days=180)

    def __init__(self):
        self.conn.execute('''
			CREATE TABLE IF NOT EXISTS log (
			  ID         INTEGER PRIMARY KEY,
			  TIMESTAMP  INT  NOT NULL,
			  USERNAME   TEXT NOT NULL,
			  FIRST_NAME TEXT,
			  LAST_NAME  TEXT,
			  MESSAGE    TEXT
			);
		''')

        self.conn.execute('''
			CREATE TABLE IF NOT EXISTS reply (
			  CHAT_ID   INT PRIMARY KEY NOT NULL,
			  USERNAME  TEXT,
			  MESSAGE   TEXT,
			  TIMESTAMP INT             NOT NULL
			)
		''')

        self.conn.commit()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.conn.commit()
        self.conn.close()

    def log(self, message):
        if message.chat.username is None:
            message.chat.username = "Anon" + str(message.chat_id)

        self.conn.execute("INSERT INTO log (TIMESTAMP, USERNAME, FIRST_NAME, LAST_NAME, MESSAGE) VALUES(?,?,?,?,?)", (
            message.date.timestamp(),
            message.chat.username,
            message.chat.first_name,
            message.chat.last_name,
            message.text))
        self.conn.commit()

    def remember(self, message):
        self.forget(message.chat_id)
        self.conn.execute("INSERT INTO reply VALUES(?,?,?,?)", (
            message.chat_id,
            message.chat.username,
            message.text if message.text is not None else "",
            message.date.timestamp()))
        self.conn.commit()

    def forget(self, chat_id):
        self.conn.execute("DELETE FROM reply WHERE CHAT_ID=?", [str(chat_id)])
        self.conn.commit()

    def get(self, message):
        sql = "SELECT MESSAGE FROM reply WHERE CHAT_ID=?"
        self.cursor.execute(sql, [str(message.chat_id)])
        self.forget(message.chat_id)
        return self.cursor.fetchone()

    def stats_total_count(self):
        sql = "SELECT count(*) FROM log"
        self.cursor.execute(sql)
        return self.cursor.fetchone()[0]

    def stats_total_count_distinct(self):
        sql = "SELECT count(DISTINCT username) FROM log"
        self.cursor.execute(sql)
        return self.cursor.fetchone()[0]

    def stats_total_count_month(self):
        sql = "SELECT count(*) FROM log WHERE TIMESTAMP > strftime('%s', 'now', '-1 month', 'localtime')"
        self.cursor.execute(sql)
        return self.cursor.fetchone()[0]

    def stats_total_count_distinct_month(self):
        sql = "SELECT count(DISTINCT USERNAME) FROM log WHERE TIMESTAMP > strftime('%s', 'now', '-1 month', 'localtime')"
        self.cursor.execute(sql)
        return self.cursor.fetchone()[0]
