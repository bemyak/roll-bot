#!/usr/bin/python3

import os
import re
import sys
import atexit
import logging
import configparser

from telegram.ext import Job, Filters, MessageHandler
from telegram.ext import Updater
from telegram.ext import CommandHandler

from Database import Database
from Repos import Repos
from Parser import Parser
from DungeonBot import DungeonBot

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger()


def str2bool(v):
	return v.lower() in ("yes", "true", "t", "1")


config_name = 'bot.conf'
config_section = 'root'
config = configparser.ConfigParser()
try:
	with open(config_name, 'r') as f:
		config_string = '[' + config_section + ']\n' + f.read()
	config.read_string(config_string)
except IOError:
	logger.warning("Config file not found! Hope you've set environment variables...")


def get_var(name, default_value=None):
	param = 'BOT_' + name.upper()
	env = os.environ.get(param)
	if not env:
		try:
			return config[config_section][param]
		except KeyError:
			logger.warning("No, you didn't set " + param + " variable")
			return default_value
	else:
		return env


host = get_var("host", "127.0.0.1")
port = int(get_var("port", "8443"))
token = get_var("token")

if not token:
	logger.critical("Token not set!")
	sys.exit()

debug = str2bool(get_var("debug", "True"))

webhook_path = ""
if not debug:
	webhook_path_base = get_var("url")
	if not host and not webhook_path_base:
		logger.critical("Environment variables not set!")
		sys.exit()
	elif not webhook_path_base:
		webhook_path_base = 'http://' + host + ":" + str(port)

	if webhook_path_base.endswith("/"):
		webhook_path = webhook_path_base + token
	else:
		webhook_path = webhook_path_base + "/" + token

updater = Updater(token=token)
atexit.register(updater.stop)
dispatcher = updater.dispatcher

if debug:
	logger.info("Starting in debug mode...")
	logger.setLevel(level=logging.DEBUG)
	updater.start_polling()
else:
	logger.info("Starting in server mode...")
	updater.start_webhook(listen=host,
	                      port=port,
	                      url_path=re.search('[^/]/([^/].*)', webhook_path).group(1))
	updater.bot.set_webhook(webhook_path)

jobber = updater.job_queue
parser = Parser()
database = Database()
dungeonbot = DungeonBot(parser, database)
repos = Repos(parser.update)
jobber.put(Job(repos.update, 86400.0))
dispatcher.add_handler(CommandHandler('roll', dungeonbot.roll))
dispatcher.add_handler(CommandHandler('spell', dungeonbot.spell))
dispatcher.add_handler(CommandHandler('item', dungeonbot.item))
dispatcher.add_handler(CommandHandler('mm', dungeonbot.monster))
dispatcher.add_handler(CommandHandler('about', dungeonbot.about))
dispatcher.add_handler(CommandHandler('start', dungeonbot.about))
dispatcher.add_handler(CommandHandler('stats', dungeonbot.stats))
dispatcher.add_handler(MessageHandler(Filters.command, dungeonbot.unknown_cmd))
dispatcher.add_handler(MessageHandler(Filters.text, dungeonbot.no_cmd))
logger.info("Bot started")
