import logging
import os
import xml.etree.ElementTree as ET
from fuzzywuzzy import fuzz
from operator import itemgetter


class Parser:
	logger = logging.getLogger()

	os.chdir(os.path.dirname(__file__))
	base_path = os.path.join(os.getcwd(), "dnd")

	spell_dir = os.path.join(base_path, "Spells")
	item_dir = os.path.join(base_path, "Items")
	monsters_dir = os.path.join(base_path, "Bestiary")
	russian_monsters_files = os.path.join(base_path, "Russian", "Monster Manual Bestiary.xml")
	russian_spell_files = os.path.join(base_path, "Russian", "Russian Spells.xml")

	spells = []
	items = []
	monsters = []

	russian_monsters = []
	russian_spells = []

	linked_tags = [["ac", "hp"], ["str", "dex", "con"], ["int", "wis", "cha"]]
	hidden_tags = ["text", "description"]
	skipped_tags = ["attack", "slots", "spells", "roll", "modifier"]
	text_skipped = ["Rarity:"]

	def update(self):
		spell_files = list(map(lambda file: os.path.join(self.spell_dir, file), os.listdir(self.spell_dir)))
		item_files = list(map(lambda file: os.path.join(self.item_dir, file), os.listdir(self.item_dir)))
		monster_files = list(map(lambda file: os.path.join(self.monsters_dir, file), os.listdir(self.monsters_dir)))

		self.spells = self.create_list(spell_files)
		self.items = self.create_list(item_files)
		self.monsters = self.create_list(monster_files)
		self.russian_monsters = self.create_list([self.russian_monsters_files])
		self.russian_spells = self.create_list([self.russian_spell_files])

	@staticmethod
	def find(string, some_list):
		scores = []
		string = string.lower()
		for i in range(len(some_list)):
			scores.append([i,
			               fuzz.partial_ratio(some_list[i][0].text.lower(), string),
			               fuzz.ratio(some_list[i][0].text.lower(), string)])
		sorted_scores = sorted(scores, key=itemgetter(1, 2), reverse=True)
		best_matches = []
		i = 0
		while True:
			current_score = sorted_scores[i]
			best_matches.append(current_score[0])
			i = i + 1
			if i == len(sorted_scores) or sorted_scores[i][1] < current_score[1] or sorted_scores[i][2] < 60:
				break
		result = []
		name_list = []
		for match in best_matches:
			if some_list[match][0].text in name_list:
				continue
			name_list.append(some_list[match][0].text)
			result.append(Parser.format(some_list[match]))
		return result

	@staticmethod
	def create_list(files):
		item_list = []
		for file in files:
			root = ET.parse(file).getroot()
			for item in root:
				item_list.append(item)
		return item_list

	@staticmethod
	def format(xml_object):
		string = ""
		prev_tag = ""
		for element in xml_object:
			need_tag = True
			need_return = True
			if len(list(element.iter())) > 1:
				if prev_tag != element.tag:
					string += "\n\n*" + element.tag + "s :*"
				string += Parser.format_sub(element)
				prev_tag = element.tag
				continue
			if element.tag is None \
					or element.text is None \
					or element.tag in Parser.skipped_tags \
					or Parser.is_bad(element.text):
				prev_tag = element.tag
				continue
			if element.tag in Parser.hidden_tags:
				need_tag = False

			for linked_list in Parser.linked_tags:
				if element.tag in linked_list and prev_tag in linked_list:
					need_return = False
					break

			if need_return:
				string += "\n"
			else:
				string += "\t"

			if need_tag:
				string += "*" + element.tag + ":* " + element.text
			else:
				string += "\n\t" + element.text
			prev_tag = element.tag
		return string

	@staticmethod
	def format_sub(element):
		string = ""
		for item in element:
			if item.tag is None \
					or item.text is None \
					or item.tag in Parser.skipped_tags \
					or Parser.is_bad(item.text):
				continue
			if item.tag == "name":
				string += "\n\n\t_" + item.text + "_: "
			else:
				string += item.text
		return string

	@staticmethod
	def is_bad(text):
		for bad_text in Parser.text_skipped:
			if bad_text in text:
				return True
		return False
