import logging
import random
import re

import telegram
from telegram import InlineKeyboardMarkup, InlineKeyboardButton


class DungeonBot:
	logger = logging.getLogger()
	dice_types = ["d20", "d12", "d10", "d8", "d6", "d4"]

	default_keyboard = telegram.ReplyKeyboardMarkup([['/roll', '/mm'], ['/spell', '/item']])
	no_keyboard = telegram.ReplyKeyboardRemove()

	message_breakers = [":", " ", "\n"]

	about_text = """
I'm the Bot. The Dungeon Bot!
I can help you with your Dungeons & Dragons game.
I can:

/roll - roll a die. By default I have d20, but you can give me any number of dices! ex.: `/roll 2d6 +5`

/mm - search for a monster. I'll look in every book in Candlekeep and find at least one. ex.: `/mm tarasque`

/spell - search for a spell. I'll ask Elminster personally about it. ex.: `/spell fireball`

/item - search for an item. I'll cast Legend Lore spell to know what it is. ex.: `/item bag of holding`

My code is open like your brain for the mind slayer!
You can get it here (code, not brain): https://gitlab.com/bemyak/roll-bot
Suggestions and contributions are welcome.  
	"""

	no_cmd_text = "I don't want to talk."
	unknown_cmd_text = "I don't know that command. See /about to learn how I can help you."
	more_results_text = "There are more results, but I won't show them to you.\nTry to use more specific query."
	ask_name_text = "Which one? Give me the name!"

	def __init__(self, parser, database):
		self.parser = parser
		self.database = database

	def __run_parser(self, bot, update, item_list):
		msg = self.__get_message(update.message.text)
		if not msg:
			self.database.remember(update.message)
			self.reply(bot, update, self.ask_name_text)
			return
		items = self.parser.find(self.__get_message(update.message.text), item_list)
		for i in range(len(items)):
			self.reply(bot, update, str(items[i]))
			if i >= 4:
				self.reply(bot, update, self.more_results_text)
				return

	def spell(self, bot, update):
		self.database.log(update.message)
		if self.__is_english(update.message.text):
			self.__run_parser(bot, update, self.parser.spells)
		else:
			self.__run_parser(bot, update, self.parser.russian_spells)

	def item(self, bot, update):
		self.database.log(update.message)
		self.__run_parser(bot, update, self.parser.items)

	def monster(self, bot, update):
		self.database.log(update.message)
		if self.__is_english(update.message.text):
			self.__run_parser(bot, update, self.parser.monsters)
		else:
			self.__run_parser(bot, update, self.parser.russian_monsters)

	def roll(self, bot, update):
		self.database.log(update.message)
		message = self.__get_message(update.message.text)
		self.reply(bot, update, self.__roll(message))

	def about(self, bot, update):
		self.database.log(update.message)
		buttons = InlineKeyboardMarkup(
			[[InlineKeyboardButton(text="Source code", url="https://gitlab.com/bemyak/roll-bot"),
			  InlineKeyboardButton(text="Buy me a coffee", url="https://www.paypal.me/bemyak/1"),
			  InlineKeyboardButton(text="Chat with me", url="https://t.me/bemyak")]])
		bot.send_message(chat_id=update.message.chat_id, text=self.about_text, reply_markup=buttons,
		                 parse_mode='Markdown', disable_web_page_preview=True)

	def no_cmd(self, bot, update):
		tupled = self.database.get(update.message)
		if not tupled:
			bot.send_message(chat_id=update.message.chat_id, text=self.no_cmd_text, reply_markup=self.default_keyboard)
			return
		command = tupled[0].strip()
		update.message.text = command + " " + update.message.text
		if "roll" in command:
			self.roll(bot, update)
		elif "mm" in command:
			self.monster(bot, update)
		elif "spell" in command:
			self.spell(bot, update)
		elif "item" in command:
			self.item(bot, update)

	def unknown_cmd(self, bot, update):
		self.reply(bot, update, self.unknown_cmd_text)

	def stats(self, bot, update):
		reply = "Total messages: " + str(self.database.stats_total_count()) + "\n"
		reply += "Unique users: " + str(self.database.stats_total_count_distinct()) + "\n"
		reply += "Messages since last mount: " + str(self.database.stats_total_count_month()) + "\n"
		reply += "Unique users since last mount: " + str(self.database.stats_total_count_distinct_month())
		self.reply(bot, update, reply)

	def reply(self, bot, update, text):
		for msg in DungeonBot.__split(text):
			bot.send_message(chat_id=update.message.chat_id, text=msg,
			                 reply_markup=self.default_keyboard, parse_mode='Markdown')

	@staticmethod
	def __split(text):
		if len(text) >= 4096:
			last_index = max(map(lambda separator: text.rfind(" ", 0, 4096), DungeonBot.message_breakers))
			good_part = text[:last_index]
			bad_part = text[last_index + 1:]
			return [good_part] + DungeonBot.__split(bad_part)
		else:
			return [text]

	@staticmethod
	def __roll(s):
		separators = ["d", "к", "д"]
		dice_regex = '((\\d+)?[' + ''.join(separators) + ']\\d+)'
		if not bool(s) == 1:
			s = "1d20"
		result = 0
		result_string = []
		for token in re.findall(dice_regex, s):
			try:
				token_sum, string = DungeonBot.__parse_die(token[0], separators)
				result += token_sum
				result_string.append(string)
			except TooManyDices:
				return "I don't have so many dices!"
			except TooManySides:
				return "I don't have such dice!"
		for digit in re.findall(r'\b(\d+)\b', s):
			result += int(digit)
			result_string.append(digit)
		return '%s = %s' % (' + '.join(result_string), result)

	@staticmethod
	def __parse_die(src, separators):
		count, base = re.split('[' + ''.join(separators) + ']', src)
		if int(count) > 100:
			raise TooManyDices()
		if int(base) > 1000:
			raise TooManySides()
		if not count:
			count = 1
		rand_sum = 0
		arr = []
		for _ in range(int(count)):
			rand = random.randint(1, int(base))
			arr.append(rand)
			rand_sum += rand
		return rand_sum, "%s(%s)" % (src, ' + '.join(map(str, arr)))

	@staticmethod
	def __get_message(string):
		words = string.split(" ", 1)
		words.pop(0)
		return " ".join(words).strip()

	@staticmethod
	def __is_english(string):
		return len(string) == len(string.encode())


class TooManyDices(Exception):
	pass


class TooManySides(Exception):
	pass
